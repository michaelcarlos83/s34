const express = require('express')

const app = express()

const port = 4000


app.use(express.json())

app.use(express.urlencoded({extended: true}))

app.listen(port, () => console.log(`Server is running at localhost:${port}`))


//GET
app.get("/home", (request, response) => {
	response.send('Welcome to the homepage!')
})


//GET 2
let arrayObj =[
{
	firstName: "Michael",
	lastName: "Carlos"
},
{
	firstName: "John",
	lastName: "Cena"
} 
]



app.get("/users", (request, response) => {
	response.send(arrayObj)
 
})


app.delete("delete-user", (request, response) => {
  for(let i = 0; i < arrayObj.length; i++){

   if(request.body.firstName == arrayObj[i].firstName){

   	arrayObj.pop(arrayObj)
   	response.send(`User ${request.body.firstName} has been deleted`)
   	break
   }else {
   	response.send(`User does not exist`)

   }
}   
})

